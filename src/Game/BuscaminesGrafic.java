package Game;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

import Core.Board;
import Core.Window;

public class BuscaminesGrafic {

	private static Scanner input = new Scanner(System.in);
	
	private static List<String> ganadors = new ArrayList<String>();
	private static Board board = new Board();
	private static Window window = new Window(board);

	public static void main(String[] args) {
		//{{ Opciones
		String nom = "";
		int files = 0, cols = 0, quantDeMines = 0;
		char[][] matOculta = null;
		char[][] matVisible = null;
		boolean buclePrincipal = true, jaHaPreparat = false;

		board.setColorbackground(0xb1adad);
		window.setSize(800, 800);
		//}}
		
		while(buclePrincipal) {
			System.out.format(
					"%s\n%s\n%s\n%s\n%s\n",
					"1. Mostrar Ajuda",
					"2. Opcions",
					"3. Jugar partida",
					"4. Veure Rankings",
					"0. Sortir"
			);
			
			switch (input.nextLine()) {
				case "1":
					mostrarAjuda();
					break;
				case "2":
					String[] outOpcions = opcions();
					nom = outOpcions[0];
					files = Integer.parseInt(outOpcions[1]);
					cols = Integer.parseInt(outOpcions[2]);
					quantDeMines = Integer.parseInt(outOpcions[3]);
					jaHaPreparat = true;
					break;
				case "3":
					if(jaHaPreparat) {
						inicialitzarGUI();
						jugarPartida(matOculta, matVisible, files, cols, quantDeMines, nom);
					}else
						System.out.println("\nPrimer has d'anar a la oció 2. Opcions");
					break;
				case "4":
					veureRankings();
					break;
				case "0":
					buclePrincipal = false;
					break;
		
				default:
					System.out.println("\nEntre el 0 y el 4!");
					break;
				
			}
			System.out.println();
		}
	}

	private static void mostrarAjuda() {
		TableGenerator table = new TableGenerator();
		List<String> headersList = new ArrayList<>();
        headersList.add("Benvingut al tutorial: MineSweeper per idiotes");
        List<List<String>> rowsList = new ArrayList<>();
        
        List<String> row = new ArrayList<>();
        row.add("Minesweeper és un joc creat per Robert Donner al 1989.");
        rowsList.add(row); 
        List<String> row2 = new ArrayList<>();
        row2.add("L'objetiu del joc és aclarir un camp de mines sense detonar cap.");
        rowsList.add(row2);
        List<String> row3 = new ArrayList<>();
        row3.add("9 = Sense aclarir   0-8 = Bombes properes");
        rowsList.add(row3);
        
        String a = table.generateTable(headersList, rowsList);
		System.out.println(a);        
		
	}

	private static void inicialitzarGUI() {
        board.setColorbackground(0xb1adad);  //color del fons (si els colors estan desactivats). Els colors estan donats en hexa, i comencen per 0x (forma d’indicar de que es tracta d’un nombre en hexa)
        board.setActimgbackground(false);  //activar imatges
        String[] lletres = {"","1","2","3","4","5","6","7","8","*","BOMBA"};  //què s'ha d'escriure en cada casella en base al nombre
        board.setText(lletres);  //passar la llista feta al taulell
        int[] colorlletres = {0x0000FF,0x00FF00,0xFFFF00,0xFF0000,0xFF00FF,0x00FFFF,0x521b98,0xFFFFFF,0xFF8000,0x7F00FF,0xFFFFFF,0xFFFFFF};   //una llista de colors de lletres. La primera posició serà el color que correspongui al nombre 0, i així. 
        board.setColortext(colorlletres);  //passar la llista feta al taulell
        String[] etiquetes2={"Mines: "+0};    //crear etiquetes per al taulell. Les etiquetes son text que sortirà a la dreta del taulell
        window.setLabels(etiquetes2);  //actualitzar les etiquetes
        window.setActLabels(true);   //activar l’existencia d’etiquetes
        window.setTitle("Cercamines");  //títol del programa.
	}
		
	private static void veureRankings() {
		System.out.println("Els ganadors son els seguents: ");
		TableGenerator table = new TableGenerator();
        List<String> headersList = new ArrayList<>();
        headersList.add("ID");
        headersList.add("Nom");
        headersList.add("Files x Columnes");
        headersList.add("Bombes");
        List<List<String>> rowsList = new ArrayList<>();
		for (int i = 0; i < ganadors.size(); i++) {
            List<String> row = new ArrayList<>(Arrays.asList(ganadors.get(i).split(";"))); 
            row.add(0, (i+1)+"");
            System.out.println(row);
            rowsList.add(row);
		}
        System.out.println(table.generateTable(headersList, rowsList));
	}

	private static void jugarPartida(char[][] matOculta, char[][] matVisible, int files, int cols, int quantDeMines, String nom) {
		matVisible = inicializarCamp(matVisible, files, cols);
		matOculta = inicialitzarMines(matOculta, files, cols, quantDeMines);
		boolean partidaEnCurs = true;
		boolean ganado = false;
		visualitzarCamp(matVisible);
		int[][] toDraw = charArrayToIntArray(matVisible);
		board.draw(toDraw, 't');

		while(partidaEnCurs) {
			System.out.print("X: ");
			int coordX = demanarcoords();
			System.out.println();
			System.out.print("Y: ");
			int coordY = demanarcoords();
			System.out.println();
			matVisible = descobrir(coordX, coordY, matOculta, matVisible);
			char d = matVisible[coordX][coordY];
			boolean[] out = partidaAcabada(d, quantDeMines, matVisible); // {partidaencurso, ganado}
			partidaEnCurs = out[0];
			ganado = out[1];
			visualitzarCamp(matVisible);
			toDraw = charArrayToIntArray(matVisible);
			board.draw(toDraw, 't');
		}
		
		fiPartida(ganado, nom, files, cols, quantDeMines);
	}

	private static int[][] charArrayToIntArray(char[][] matVisible) {
		int[][] aux = new int[matVisible.length][matVisible[0].length];
		for (int i = 0; i < aux.length; i++) {
			for (int j = 0; j < aux[0].length; j++) {
				if(matVisible[i][j] == '?') {
					aux[i][j] = '9';
				}else {
					aux[i][j] = Integer.parseInt(matVisible[i][j]+"");
				}
			}
		}
		return aux;
	}

	private static void fiPartida(boolean ganado, String nom, int files, int columnes, int quantDeMines) {
		if(ganado) {
			System.out.println("Has guanyat!");
			ganadors.add(nom+";"+files+"x"+columnes+";"+quantDeMines);
		}else {
			System.out.println("Ha explotat una bomba");
		}
		input.nextLine();
	}

	private static boolean[] partidaAcabada(char d, int quantDeMines, char[][] matVisible) {
		if(d == '?') {
			boolean[] out = {false, false};
			return out;
		}else {
			int cantSenseComp= 0;
			for (int i = 0; i < matVisible.length; i++) {
				for (int j = 0; j < matVisible[0].length; j++) {
					if(matVisible[i][j] == '9')
						cantSenseComp++;
				}
			}
			if(cantSenseComp == quantDeMines) {
				boolean[] out = {false, true};
				return out;
			}else {
				boolean[] out = {true, false};
				return out;
			}
		}
	}

	private static char[][] descobrir(int coordX, int coordY, char[][] matOculta, char[][] matVisible) {
		char out = destapar(coordX, coordY, matOculta);
		matVisible[coordX][coordY] = out;
		try {
			if(matVisible[coordX][coordY] == '0') {
				for (int i = 0; i < 2; i++) {
					if(matVisible[coordX-1][coordY] == '9') {
						out = destapar(coordX-1, coordY, matOculta);
						if(out == '0')
							descobrir(coordX-1, coordY, matOculta, matVisible);
					}
					if(matVisible[coordX+1][coordY] == '9') {
						out = destapar(coordX+1, coordY, matOculta);
						if(out == '0')
							descobrir(coordX+1, coordY, matOculta, matVisible);				
					}
					if(matVisible[coordX][coordY-1] == '9') {
						out = destapar(coordX, coordY-1, matOculta);
						if(out == '0')
							descobrir(coordX, coordY-1, matOculta, matVisible);
					}
					if(matVisible[coordX][coordY+1] == '9') {
						out = destapar(coordX, coordY+1, matOculta);
						if(out == '0')
							descobrir(coordX, coordY+1, matOculta, matVisible);
					}
					matVisible[coordX][coordY] = out;
				}
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		

		
//		int maxIterations = matOculta.length*matOculta[0].length;
//		for (int x = 0; x < maxIterations; x++) {
//			for (int i = 0; i < matVisible.length; i++) {
//				for (int j = 0; j < matVisible[0].length; j++) {
//					if(matVisible[i][j] == '0') {
//						if(i == 0) {
//							if(j == 0) {
//								out = destapar(i, j+1, matOculta);
//								matVisible[i][j+1] = out;
//								
//								out = destapar(i+1, j, matOculta);
//								matVisible[i+1][j] = out;
//							}else if(j == matOculta[0].length-1) {
//								out = destapar(i, j-1, matOculta);
//								matVisible[i][j-1] = out;
//								
//								out = destapar(i+1, j, matOculta);
//								matVisible[i+1][j] = out;
//								
//							}else {
//								out = destapar(i, j-1, matOculta);
//								matVisible[i][j-1] = out;
//								
//								out = destapar(i, j+1, matOculta);
//								matVisible[i][j+1] = out;
//								
//								out = destapar(i+1, j, matOculta);
//								matVisible[i+1][j] = out;
//								
//							}
//						}else if(i == matOculta.length-1) {
//							if(j == 0) {
//								out = destapar(i-1, j, matOculta);
//								matVisible[i-1][j] = out;
//																
//								out = destapar(i, j+1, matOculta);
//								matVisible[i][j+1] = out;
//								
//							}else if(j == matOculta[0].length-1) {								
//								out = destapar(i-1, j, matOculta);
//								matVisible[i-1][j] = out;
//								
//								out = destapar(i, j-1, matOculta);
//								matVisible[i][j-1] = out;
//								
//							}else {
//								out = destapar(i-1, j, matOculta);
//								matVisible[i-1][j] = out;
//								
//								out = destapar(i, j-1, matOculta);
//								matVisible[i][j-1] = out;
//								
//								out = destapar(i, j+1, matOculta);
//								matVisible[i][j+1] = out;
//								
//							}
//						}else {
//							if(j == 0) {
//								out = destapar(i-1, j, matOculta);
//								matVisible[i-1][j] = out;
//								
//								out = destapar(i, j+1, matOculta);
//								matVisible[i][j+1] = out;
//								
//								out = destapar(i+1, j, matOculta);
//								matVisible[i+1][j] = out;
//								
//							}else if(j == matOculta[0].length-1) {								
//								out = destapar(i-1, j, matOculta);
//								matVisible[i-1][j] = out;
//								
//								out = destapar(i, j-1, matOculta);
//								matVisible[i][j-1] = out;
//								
//								out = destapar(i+1, j, matOculta);
//								matVisible[i+1][j] = out;
//							}else {
//								out = destapar(i-1, j, matOculta);
//								matVisible[i-1][j] = out;
//																
//								out = destapar(i, j-1, matOculta);
//								matVisible[i][j-1] = out;
//								
//								out = destapar(i, j+1, matOculta);
//								matVisible[i][j+1] = out;
//								
//								out = destapar(i+1, j, matOculta);
//								matVisible[j+1][j] = out;
//								
//							}
//						}
//					}
//				maxIterations--;
//				}
//			}
//		}


		return matVisible;
	}

	private static char destapar(int xfila, int ycols, char[][] matOculta) {
		if(matOculta[xfila][ycols] == '1') {
			return '?';
		}else {
			char minas;
			int nmin=0;
			
			for (int i = xfila-1; i <= xfila+1; i++) {
				for (int j = ycols-1; j <= ycols+1; j++) {
					nmin+= check(matOculta, i, j);
				}
				
			}
			minas = (nmin+"").charAt(0);
			return minas;
		}
	}
	
	private static int check(char[][] matO, int f, int c) {
		if(f<0||c<0||f>=matO.length||c>=matO[0].length) {
			return 0;
		}
		if(matO[f][c]=='1') {
			return 1;
		}else {
			return 0;
		}
	}

	private static int demanarcoords() {
		return input.nextInt();
	}

	private static char[][] inicialitzarMines(char[][] matOculta, int files, int cols, int quantDeMines) {
		Random r = new Random();
		matOculta = new char[files][cols];
		float perc = 10;
		for (int i = 0; i < matOculta.length; i++) {
			for (int j = 0; j < matOculta[0].length; j++) {
				matOculta[i][j] = '0';
			}
		}
		
		while(quantDeMines > 0) {
			for (int i = 0; i < matOculta.length; i++) {
				for (int j = 0; j < matOculta[0].length; j++) {
					float randomNum = r.nextFloat()*100;
					if(perc>randomNum && quantDeMines > 0 && matOculta[i][j] != '1') {
						matOculta[i][j] = '1';
						quantDeMines--;
					}
				}
			}
		}
		return matOculta;
	}

	private static char[][] inicializarCamp(char[][] matVisible, int files, int cols) {
		matVisible = new char[files][cols];
		for (int i = 0; i < matVisible.length; i++) {
			for (int j = 0; j < matVisible[0].length; j++) {
				matVisible[i][j] = '9';
			}
		}
		return matVisible;		
	}

	private static String[] opcions() {
		String[] out = new String[4];
		System.out.print("Nom: "); out[0] = input.nextLine();
		System.out.print("Files y columnes: "); out[1] = input.nextInt()+""; out[2] = input.nextInt()+"";
		System.out.print("Quantitat de mines: "); out[3] = input.nextInt()+"";
		return out;
	}
	
	private static void visualitzarCamp(char[][] mat) {
		for (int i = 0; i < mat.length; i++) {
			if(i == 0) {
				System.out.print("   ");
				for (int j = 0; j < mat[0].length; j++) {
					System.out.print(j+" ");
					
				}
				System.out.print("\n   ");
				for (int j = 0; j < mat[0].length; j++) {
					System.out.print("─ ");
					
				}
				System.out.println();
			}
			System.out.print(i+"| ");
			for (int j = 0; j < mat[0].length; j++) {
				System.out.print(mat[i][j]+" ");
			}System.out.println();
			
		}
	}

}
