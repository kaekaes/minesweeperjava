package Game;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class Buscamines {

	private static Scanner input = new Scanner(System.in);
	
	private static List<String> ganadors = new ArrayList<String>();
	
	public static void main(String args) {
		//{{ Opciones
		String nom = "";
		int files = 0, cols = 0, quantDeMines = 0;
		char[][] matOculta = null;
		char[][] matVisible = null;
		boolean buclePrincipal = true, jaHaPreparat = false;
		//}}
		
		while(buclePrincipal) {
			System.out.format(
					"%s\n%s\n%s\n%s\n%s\n",
					"1. Mostrar Ajuda",
					"2. Opcions",
					"3. Jugar partida",
					"4. Veure Rankings",
					"0. Sortir"
			);
			
			switch (input.nextLine()) {
				case "1":
					mostrarAjuda();
					break;
				case "2":
					String[] outOpcions = opcions();
					nom = outOpcions[0];
					files = Integer.parseInt(outOpcions[1]);
					cols = Integer.parseInt(outOpcions[2]);
					quantDeMines = Integer.parseInt(outOpcions[3]);
					jaHaPreparat = true;
					break;
				case "3":
					if(jaHaPreparat)
						jugarPartida(matOculta, matVisible, files, cols, quantDeMines, nom);
					else
						System.out.println("\nPrimer has d'anar a la oció 2. Opcions");
					break;
				case "4":
					veureRankings();
					break;
				case "0":
					buclePrincipal = false;
					break;
		
				default:
					System.out.println("\nEntre el 0 y el 4!");
					break;
				
			}
			System.out.println();
		}
	}

	private static void mostrarAjuda() {
		TableGenerator table = new TableGenerator();
		List<String> headersList = new ArrayList<>();
        headersList.add("Benvingut al tutorial: MineSweeper per idiotes");
        List<List<String>> rowsList = new ArrayList<>();
        
        List<String> row = new ArrayList<>();
        row.add("Minesweeper és un joc creat per Robert Donner al 1989.");
        rowsList.add(row); 
        List<String> row2 = new ArrayList<>();
        row2.add("L'objetiu del joc és aclarir un camp de mines sense detonar cap.");
        rowsList.add(row2);
        List<String> row3 = new ArrayList<>();
        row3.add("# = Sense aclarir   0-8 = Bombes properes");
        rowsList.add(row3);
        
        String a = table.generateTable(headersList, rowsList);
		System.out.println(a);        
		
	}

	private static void veureRankings() {
		System.out.println("Els ganadors son els seguents: ");
		TableGenerator table = new TableGenerator();
        List<String> headersList = new ArrayList<>();
        headersList.add("ID");
        headersList.add("Nom");
        headersList.add("Files x Columnes");
        headersList.add("Bombes");
        List<List<String>> rowsList = new ArrayList<>();
		for (int i = 0; i < ganadors.size(); i++) {
            List<String> row = new ArrayList<>(Arrays.asList(ganadors.get(i).split(";"))); 
            row.add(0, (i+1)+"");
            System.out.println(row);
            rowsList.add(row);
		}
        System.out.println(table.generateTable(headersList, rowsList));
	}

	private static void jugarPartida(char[][] matOculta, char[][] matVisible, int files, int cols, int quantDeMines, String nom) {
		matVisible = inicializarCamp(matVisible, files, cols);
		matOculta = inicialitzarMines(matOculta, files, cols, quantDeMines);
		boolean partidaEnCurs = true;
		boolean ganado = false;
		visualitzarCamp(matVisible);
		while(partidaEnCurs) {
			System.out.print("X: ");
			int coordX = demanarcoords();
			System.out.println();
			System.out.print("Y: ");
			int coordY = demanarcoords();
			System.out.println();
			matVisible = descobrir(coordX, coordY, matOculta, matVisible);
			char d = matVisible[coordX][coordY];
			boolean[] out = partidaAcabada(d, quantDeMines, matVisible); // {partidaencurso, ganado}
			partidaEnCurs = out[0];
			ganado = out[1];
			visualitzarCamp(matVisible);
		}
		
		fiPartida(ganado, nom, files, cols, quantDeMines);
	}

	private static void fiPartida(boolean ganado, String nom, int files, int columnes, int quantDeMines) {
		if(ganado) {
			System.out.println("Has guanyat!");
			ganadors.add(nom+";"+files+"x"+columnes+";"+quantDeMines);
		}else {
			System.out.println("Ha explotat una bomba");
		}
		input.nextLine();
	}

	private static boolean[] partidaAcabada(char d, int quantDeMines, char[][] matVisible) {
		if(d == '?') {
			boolean[] out = {false, false};
			return out;
		}else {
			int cantSenseComp= 0;
			for (int i = 0; i < matVisible.length; i++) {
				for (int j = 0; j < matVisible[0].length; j++) {
					if(matVisible[i][j] == '#')
						cantSenseComp++;
				}
			}
			if(cantSenseComp == quantDeMines) {
				boolean[] out = {false, true};
				return out;
			}else {
				boolean[] out = {true, false};
				return out;
			}
		}
	}

	private static char[][] descobrir(int coordX, int coordY, char[][] matOculta, char[][] matVisible) {
		char out = destapar(coordX, coordY, matOculta);
		matVisible[coordX][coordY] = out;
		int maxIterations = matOculta.length*matOculta[0].length;
		for (int x = 0; x < maxIterations; x++) {
			for (int i = 0; i < matVisible.length; i++) {
				for (int j = 0; j < matVisible[0].length; j++) {
					if(matVisible[i][j] == '0') {
						if(i == 0) {
							if(j == 0) {
								out = destapar(i, j+1, matOculta);
								matVisible[i][j+1] = out;
								
								out = destapar(i+1, j, matOculta);
								matVisible[i+1][j] = out;
								
								out = destapar(i+1, j+1, matOculta);
								matVisible[i+1][j+1] = out;
	
								
							}else if(j == matOculta[0].length-1) {
								out = destapar(i, j-1, matOculta);
								matVisible[i][j-1] = out;
								
								out = destapar(i+1, j-1, matOculta);
								matVisible[i+1][j-1] = out;
								
								out = destapar(i+1, j, matOculta);
								matVisible[i+1][j] = out;
								
							}else {
								out = destapar(i, j-1, matOculta);
								matVisible[i][j-1] = out;
								
								out = destapar(i, j+1, matOculta);
								matVisible[i][j+1] = out;
								
								out = destapar(i+1, j-1, matOculta);
								matVisible[i+1][j-1] = out;
								
								out = destapar(i+1, j, matOculta);
								matVisible[i+1][j] = out;
								
								out = destapar(i+1, j+1, matOculta);
								matVisible[i+1][j+1] = out;
								
							}
						}else if(i == matOculta.length-1) {
							if(j == 0) {
								out = destapar(i-1, j, matOculta);
								matVisible[i-1][j] = out;
								
								out = destapar(i-1, j+1, matOculta);
								matVisible[i-1][j+1] = out;
								
								out = destapar(i, j+1, matOculta);
								matVisible[i][j+1] = out;
								
							}else if(j == matOculta[0].length-1) {
								out = destapar(i-1, j-1, matOculta);
								matVisible[i-1][j-1] = out;
								
								out = destapar(i-1, j, matOculta);
								matVisible[i-1][j] = out;
								
								out = destapar(i, j-1, matOculta);
								matVisible[i][j-1] = out;
								
							}else {
								out = destapar(i-1, j-1, matOculta);
								matVisible[i-1][j-1] = out;
								
								out = destapar(i-1, j, matOculta);
								matVisible[i-1][j] = out;
								
								out = destapar(i-1, j+1, matOculta);
								matVisible[i-1][j+1] = out;
								
								out = destapar(i, j-1, matOculta);
								matVisible[i][j-1] = out;
								
								out = destapar(i, j+1, matOculta);
								matVisible[i][j+1] = out;
								
							}
						}else {
							if(j == 0) {
								out = destapar(i-1, j, matOculta);
								matVisible[i-1][j] = out;
								
								out = destapar(i-1, j+1, matOculta);
								matVisible[i-1][j+1] = out;
								
								out = destapar(i, j+1, matOculta);
								matVisible[i][j+1] = out;
								
								out = destapar(i+1, j, matOculta);
								matVisible[i+1][j] = out;
								
								out = destapar(i+1, j+1, matOculta);
								matVisible[i+1][j+1] = out;
								
							}else if(j == matOculta[0].length-1) {
								out = destapar(i-1, j-1, matOculta);
								matVisible[i-1][j-1] = out;
								
								out = destapar(i-1, j, matOculta);
								matVisible[i-1][j] = out;
								
								out = destapar(i, j-1, matOculta);
								matVisible[i][j-1] = out;
								
								out = destapar(i+1, j-1, matOculta);
								matVisible[i+1][j-1] = out;
								
								out = destapar(i+1, j, matOculta);
								matVisible[i+1][j] = out;
							}else {
								out = destapar(i-1, j-1, matOculta);
								matVisible[i-1][j-1] = out;
								
								out = destapar(i-1, j, matOculta);
								matVisible[i-1][j] = out;
								
								out = destapar(i-1, j+1, matOculta);
								matVisible[i-1][j+1] = out;
								
								out = destapar(i, j-1, matOculta);
								matVisible[i][j-1] = out;
								
								out = destapar(i, j+1, matOculta);
								matVisible[i][j+1] = out;
								
								out = destapar(i+1, j-1, matOculta);
								matVisible[i+1][j-1] = out;
								
								out = destapar(i+1, j, matOculta);
								matVisible[j+1][j] = out;
								
								out = destapar(i+1, j+1, matOculta);
								matVisible[i+1][j+1] = out;
							}
						}
					}
				maxIterations--;
				}
			}
		}
		return matVisible;
	}

	private static char destapar(int xfila, int ycols, char[][] matOculta) {
		if(matOculta[xfila][ycols] == '1') {
			return '?';
		}else {
			char minas;
			int nmin=0;
			
			for (int i = xfila-1; i <= xfila+1; i++) {
				for (int j = ycols-1; j <= ycols+1; j++) {
					nmin+= check(matOculta, i, j);
				}
				
			}
			minas = (nmin+"").charAt(0);
			return minas;
			
			/*
			char minas = '0';
			if(xfila == 0) {
				if(ycols == 0) {
					if(matOculta[xfila][ycols+1] == '1')
						minas = (((Integer.parseInt(minas+""))+1)+"").charAt(0);
					if(matOculta[xfila+1][ycols] == '1')
						minas = (((Integer.parseInt(minas+""))+1)+"").charAt(0);
					if(matOculta[xfila+1][ycols+1] == '1')
						minas = (((Integer.parseInt(minas+""))+1)+"").charAt(0);
				}else if(ycols == matOculta[0].length-1) {
					if(matOculta[xfila][ycols-1] == '1')
						minas = (((Integer.parseInt(minas+""))+1)+"").charAt(0);
					if(matOculta[xfila+1][ycols-1] == '1')
						minas = (((Integer.parseInt(minas+""))+1)+"").charAt(0);
					if(matOculta[xfila+1][ycols] == '1')
						minas = (((Integer.parseInt(minas+""))+1)+"").charAt(0);					
				}else {
					if(matOculta[xfila][ycols-1] == '1')
						minas = (((Integer.parseInt(minas+""))+1)+"").charAt(0);
					if(matOculta[xfila][ycols+1] == '1')
						minas = (((Integer.parseInt(minas+""))+1)+"").charAt(0);
					if(matOculta[xfila+1][ycols-1] == '1')
						minas = (((Integer.parseInt(minas+""))+1)+"").charAt(0);	
					if(matOculta[xfila+1][ycols] == '1')
						minas = (((Integer.parseInt(minas+""))+1)+"").charAt(0);
					if(matOculta[xfila+1][ycols+1] == '1')
						minas = (((Integer.parseInt(minas+""))+1)+"").charAt(0);
				}
			}else if(xfila == matOculta.length-1) {
				if(ycols == 0) {
					if(matOculta[xfila-1][ycols] == '1')
						minas = (((Integer.parseInt(minas+""))+1)+"").charAt(0);
					if(matOculta[xfila-1][ycols+1] == '1')
						minas = (((Integer.parseInt(minas+""))+1)+"").charAt(0);
					if(matOculta[xfila][ycols+1] == '1')
						minas = (((Integer.parseInt(minas+""))+1)+"").charAt(0);
				}else if(ycols == matOculta[0].length-1) {
					if(matOculta[xfila-1][ycols-1] == '1')
						minas = (((Integer.parseInt(minas+""))+1)+"").charAt(0);
					if(matOculta[xfila-1][ycols] == '1')
						minas = (((Integer.parseInt(minas+""))+1)+"").charAt(0);
					if(matOculta[xfila][ycols-1] == '1')
						minas = (((Integer.parseInt(minas+""))+1)+"").charAt(0);					
				}else {
					if(matOculta[xfila-1][ycols-1] == '1')
						minas = (((Integer.parseInt(minas+""))+1)+"").charAt(0);
					if(matOculta[xfila-1][ycols] == '1')
						minas = (((Integer.parseInt(minas+""))+1)+"").charAt(0);
					if(matOculta[xfila-1][ycols+1] == '1')
						minas = (((Integer.parseInt(minas+""))+1)+"").charAt(0);	
					if(matOculta[xfila][ycols-1] == '1')
						minas = (((Integer.parseInt(minas+""))+1)+"").charAt(0);
					if(matOculta[xfila][ycols+1] == '1')
						minas = (((Integer.parseInt(minas+""))+1)+"").charAt(0);
				}
			}else {
				if(ycols == 0) {
					if(matOculta[xfila-1][ycols] == '1')
						minas = (((Integer.parseInt(minas+""))+1)+"").charAt(0);
					if(matOculta[xfila-1][ycols+1] == '1')
						minas = (((Integer.parseInt(minas+""))+1)+"").charAt(0);
					if(matOculta[xfila][ycols+1] == '1')
						minas = (((Integer.parseInt(minas+""))+1)+"").charAt(0);	
					if(matOculta[xfila+1][ycols] == '1')
						minas = (((Integer.parseInt(minas+""))+1)+"").charAt(0);
					if(matOculta[xfila+1][ycols+1] == '1')
						minas = (((Integer.parseInt(minas+""))+1)+"").charAt(0);
					
				}else if(ycols == matOculta[0].length-1) {
					if(matOculta[xfila-1][ycols-1] == '1')
						minas = (((Integer.parseInt(minas+""))+1)+"").charAt(0);
					if(matOculta[xfila-1][ycols] == '1')
						minas = (((Integer.parseInt(minas+""))+1)+"").charAt(0);
					if(matOculta[xfila][ycols-1] == '1')
						minas = (((Integer.parseInt(minas+""))+1)+"").charAt(0);	
					if(matOculta[xfila+1][ycols-1] == '1')
						minas = (((Integer.parseInt(minas+""))+1)+"").charAt(0);
					if(matOculta[xfila+1][ycols] == '1')
						minas = (((Integer.parseInt(minas+""))+1)+"").charAt(0);
					
				}else {
					if(matOculta[xfila-1][ycols-1] == '1')
						minas = (((Integer.parseInt(minas+""))+1)+"").charAt(0);
					if(matOculta[xfila][ycols-1] == '1')
						minas = (((Integer.parseInt(minas+""))+1)+"").charAt(0);
					if(matOculta[xfila+1][ycols-1] == '1')
						minas = (((Integer.parseInt(minas+""))+1)+"").charAt(0);	
					if(matOculta[xfila-1][ycols] == '1')
						minas = (((Integer.parseInt(minas+""))+1)+"").charAt(0);
					if(matOculta[xfila+1][ycols] == '1')
						minas = (((Integer.parseInt(minas+""))+1)+"").charAt(0);
					if(matOculta[xfila-1][ycols+1] == '1')
						minas = (((Integer.parseInt(minas+""))+1)+"").charAt(0);
					if(matOculta[xfila][ycols+1] == '1')
						minas = (((Integer.parseInt(minas+""))+1)+"").charAt(0);
					if(matOculta[xfila+1][ycols+1] == '1')
						minas = (((Integer.parseInt(minas+""))+1)+"").charAt(0);
				}
			}
			*/
		}
	}
	
	private static int check(char[][] matO, int f, int c) {
		if(f<0||c<0||f>=matO.length||c>=matO[0].length) {
			return 0;
		}
		if(matO[f][c]=='1') {
			return 1;
		}else {
			return 0;
		}
	}

	private static int demanarcoords() {
		return input.nextInt();
	}

	private static char[][] inicialitzarMines(char[][] matOculta, int files, int cols, int quantDeMines) {
		Random r = new Random();
		matOculta = new char[files][cols];
		float perc = 10;
		for (int i = 0; i < matOculta.length; i++) {
			for (int j = 0; j < matOculta[0].length; j++) {
				matOculta[i][j] = '0';
			}
		}
		
		while(quantDeMines > 0) {
			for (int i = 0; i < matOculta.length; i++) {
				for (int j = 0; j < matOculta[0].length; j++) {
					float randomNum = r.nextFloat()*100;
					if(perc>randomNum && quantDeMines > 0 && matOculta[i][j] != '1') {
						matOculta[i][j] = '1';
						quantDeMines--;
					}
				}
			}
		}
		return matOculta;
	}

	private static char[][] inicializarCamp(char[][] matVisible, int files, int cols) {
		matVisible = new char[files][cols];
		for (int i = 0; i < matVisible.length; i++) {
			for (int j = 0; j < matVisible[0].length; j++) {
				matVisible[i][j] = '#';
			}
		}
		return matVisible;		
	}

	private static String[] opcions() {
		String[] out = new String[4];
		System.out.print("Nom: "); out[0] = input.nextLine();
		System.out.print("Files y columnes: "); out[1] = input.nextInt()+""; out[2] = input.nextInt()+"";
		System.out.print("Quantitat de mines: "); out[3] = input.nextInt()+"";
		return out;
	}
	
	private static void visualitzarCamp(char[][] mat) {
		for (int i = 0; i < mat.length; i++) {
			if(i == 0) {
				System.out.print("   ");
				for (int j = 0; j < mat[0].length; j++) {
					System.out.print(j+" ");
					
				}
				System.out.print("\n   ");
				for (int j = 0; j < mat[0].length; j++) {
					System.out.print("─ ");
					
				}
				System.out.println();
			}
			System.out.print(i+"| ");
			for (int j = 0; j < mat[0].length; j++) {
				System.out.print(mat[i][j]+" ");
			}System.out.println();
			
		}
	}

}
